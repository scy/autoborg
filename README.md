# autoborg

_A wrapper around Borg Backup that simplifies things and has sane defaults._

## Status

In development, but already quite usable.
It needs some more documentation before I can consider it a "release" though, see the [0.1 milestone](https://codeberg.org/scy/autoborg/milestone/395).

## Author

Tim [@scy](https://codeberg.org/scy) Weber.
